package manel.cabezas.t2ej1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.devspark.appmsg.AppMsg;


public class MainActivity extends Activity {

    private static final String TAG="DEBUG T2EJ1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Log.d(TAG, "Llamado ON CREATED");


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Llamado ONDESTROY");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Llamado ONPAUSE");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "Llamado ONRESTART");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Llamado ONRESUME");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Llamado ONSTART");
        AppMsg.makeText(this, "Funciona", AppMsg.STYLE_INFO).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Llamado ONSTOP");
    }
}
